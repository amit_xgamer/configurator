let buttonstep;
let currentStep = 0;
let specs = ["Hardcover", "Round corners", "A5", "Gracious Grey", "-", "-", "Blanco"];
let color = "url(../leoprintingconfigurator/images/covers/grey1.svg)";
let savePos = 0;
showStep(currentStep);
// Show next step, and let the previous step dissapear
function showStep(step) {
    let products = document.getElementsByClassName("product");
    let x = document.getElementsByClassName("step");
    $(x[currentStep]).fadeIn();
    if (step === 0) {
        $('#prevbtn').hide();
        $('#intro').fadeIn();
        $("#product").hide();
        $("#cellphone").hide();
        $("#help").hide();
        $("#reset").hide();
    } else {
        $('#prevbtn').show();
        $('#intro').hide();
        $('#product').fadeIn();
        $('#cellphone').fadeIn();
        $('#help').fadeIn();
        $('#reset').fadeIn();
    }
    if (step === 1) {
        $('#stepL0').fadeIn();
    } else {
        $('#stepL0').hide();
    }
    if (currentStep === (x.length - 1)) {
        $('#nextbtn').hide();
        $("#product").fadeOut();
    } else if(currentStep == (x.length -2)){
        $('#nextbtn').hide();
        displaySpecs();
    }
    else {
        $('#nextbtn').show();
    }
    if (currentStep === 4){
        $("#product").fadeIn();
        $("#productContainer").removeClass();
        $("#productContainer").addClass("col-lg-4");
        $("#stepsRight").removeClass();
        $("#stepsRight").addClass("col-lg-4");
        $("#stepsLeft").removeClass();
        $("#stepsLeft").addClass("col-lg-2");
        $(".custom").remove();
    } else if (currentStep === 3) {
        $("#productContainer").removeClass("col-lg-3");
        $("#productContainer").addClass("col-lg-5");
        $("#stepsRight").removeClass();
        $("#stepsRight").addClass("col-lg-5");
        $("#stepsLeft").removeClass();
        $(".product").clone().appendTo(x[3]);
        $(products[1]).addClass("custom");
        $(products[1]).removeClass("product");
        $(".custom").removeAttr('id');
    }else{
        $("#stepsRight").removeClass();
        $("#stepsLeft").removeClass();
        $("#stepsLeft").addClass("col-lg-3");
        $("#stepsRight").addClass("col-lg-3");
        $("#productContainer").removeClass();
        $("#productContainer").addClass("col-lg-4");
        $(".custom").remove();
    }
    if (savePos !== 0) {
        $("#savePos").fadeIn();
    } else {
        $("#savePos").hide();
    }
    
    //Buttons 1 choice
    let questions = document.getElementsByClassName("question");
    for (let i = 0; i < questions.length; i++) {
        let buttonField = questions[i];
        let buttonUnselect = buttonField.getElementsByClassName("button");
        for (let i = 0; i < buttonUnselect.length; i++) {
            buttonUnselect[i].addEventListener("click", function () {
                let current = buttonField.getElementsByClassName("active");
                if(current[0] === undefined){
                    this.className += " active";
                }else{
                    current[0].className = current[0].className.replace("active"," ");
                    this.className += " active";
                }
               
            });
        }
    }
    // color balls
    colorBalls = document.getElementsByClassName("colorBallMenu");
    for (let i = 0; i < colorBalls.length; i++) {
        let buttonField = colorBalls[i];
        let buttonUnselect = buttonField.getElementsByClassName("colorBall");
        for (let i = 0; i < buttonUnselect.length; i++) {
            buttonUnselect[i].addEventListener("click", function () {
                let current = buttonField.getElementsByClassName("activeBall");
                    current[0].className = current[0].className.replace("activeBall", " ");
                    this.className += " activeBall";

            });
        }
    }
    //buttons toggle
    //$("button")
}
//Function to navigate between steps
function nextPrev(step) {
    let x = document.getElementsByClassName("step");
    $(x[currentStep]).hide();
    currentStep = currentStep + step;
    showStep(currentStep);

}
//Display Specs
function displaySpecs() {
    $("#coverSpan").html(specs[0]);
    $("#cornersSpan").html(specs[1]);
    $("#formatSpan").html(specs[2]);
    $("#colorSpan").html(specs[3]);
    $("#brandingSpan").html(specs[4]);
    $("#accessoriesSpan").html(specs[5]);
    $("#layoutSpan").html(specs[6]);
}
// Reset functie
function reset() {
    savePos = 0;
    //setting default values
    color = "url(../configurator/images/gloss.png) , url(../configurator/images/covers/grey1.svg)";
    specs = ["Hardcover", "Round corners", "A5", "Gracious Grey", "-", "-", "Blanco"];
    //Resetting to first step
    nextPrev(-currentStep + 1);
    showStep(currentStep);
    //resetting visual product to default
    formatVisual(a5);
    changeColor();
    $('#brandLogo').hide();
    $('#elasticBand').hide();
    $('#penLoop').hide();
    // Hiding the reset overlay
    $("#resetOverlay").hide();
    //hiding color picker popups
    $("#elasticBandMenu").fadeOut();
    $("#penLoopMenu").fadeOut();
    //Resetting accessory colors
    $('#penLoop').css('background-color', 'aliceblue');
    $('#elasticBand').css('background-color', 'aliceblue');
    // Resetting the buttons to default values
    let buttonUnselect = document.getElementsByClassName("button");
    let buttonDefault = document.getElementsByClassName("default");
    //setting active buttons to non active
    for (let i = 0; i < buttonUnselect.length; i++) {
        buttonUnselect[i].classList.remove("active");
    }
    //setting default buttons to active
    for (let i = 0; i < buttonDefault.length; i++) {
        buttonDefault[i].classList.add("active");
    }
    // Resetting the buttons to default values
    let ballUnselect = document.getElementsByClassName("colorBall");
    let ballDefault = document.getElementsByClassName("defaultBall");
    //setting active buttons to non active
    for (let i = 0; i < ballUnselect.length; i++) {
        ballUnselect[i].classList.remove("activeBall");
    }
    //setting default buttons to active
    for (let i = 0; i < ballDefault.length; i++) {
        ballDefault[i].classList.add("activeBall");
    }


}
//Reset opties
function resetDisplayOn() {
    $("#resetOverlay").fadeIn();
}
function resetDisplayOff() {
    $("#resetOverlay").fadeOut();
    showStep(currentStep);
}
