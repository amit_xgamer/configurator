<?php
	set_time_limit(0);   
	ini_set('mysql.connect_timeout','0');   
	ini_set('max_execution_time', '0'); 
	
	$server = 'localhost';
	$username = 'root';
	$password = '';
	$database = 'configurator';
	
	$mysql = new mysqli($server, $username, $password, $database);
	if($mysql->connect_error) {
		die("Connection failed: " . $mysql->connect_error);
	}	
?>