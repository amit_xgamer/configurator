<?php
require_once('config.php');

$sort = $_POST['sort'];
$query = $mysql->query("SELECT * FROM covercolor INNER JOIN prices ON covercolor.priceID = prices.priceID WHERE textureID = {$sort}");
 
$data = [];
if($query->num_rows > 0) {
	while($rows = $query->fetch_assoc()) {
		$data[] = [
			'coverPath' => $rows['coverPath'],
			'class' => $rows['class'],
			'coverName' => $rows['coverName']
		];
	} 
	echo json_encode($data);
}
?>