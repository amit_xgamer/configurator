<?php
//Database Connection
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "configurator";
$conn = mysqli_connect($servername, $username, $password, $dbname);

$sort = $_POST['sort'];
//Query setup
$sql = "SELECT * FROM covercolor INNER JOIN prices ON covercolor.priceID = prices.priceID WHERE textureID = $sort";
//Call Query
$result = mysqli_query($conn, $sql);
$data = [];
if(mysqli_num_rows($result) > 0) {
	while($rows = mysqli_fetch_assoc($result)) { 
		$data[] = [
			'coverPath' => $rows['coverPath'],
			'class' => $rows['class'],
			'coverName' => $rows['coverName']
		];
	} 
	echo json_encode($data);
}
?>