<?php
//Database Connection
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "configurator";
$conn = mysqli_connect($servername, $username, $password, $dbname);

//Query setup
$sql = "SELECT * FROM covercolor INNER JOIN prices ON covercolor.priceID = prices.priceID";
//Call Query
$result = mysqli_query($conn, $sql);
?>
<html>

<head>
    <link rel="stylesheet" href="css/style.css">
    <script src="./js/jquery-3.5.1.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="https://kit.fontawesome.com/292c831ebb.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="stylesheet">

</head>

<body>
    <!-- Reset Overlay -->
    <div id="resetOverlay" onclick="resetDisplayOff()">
        <div id="dialogBox">
            <p>Do you want to discard all your current selections? </p>
            <div id="resetButtonBox">
                <button id="confirmReset" onclick="reset()">Yes, i want to start over.</button>
                <button id="cancelReset" onclick="resetDisplayOff()">No, i want to continue with my current design</button>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-8">
                <div class="text-center">
                    <img src="images/logo.png" id="logo" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-8">
                <hr>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row h-75">
            <!-- Previous button-->
            <div class="col-lg-1">
                <div id="prevbtn" onclick="nextPrev(-1);">
                    <i class="fas fa-chevron-left fa-3x"></i>
                </div>
            </div>
            <!-- steps left-->
            <div class="col-lg-3" id="stepsLeft">
                <div class="stepL" id="stepL0">
                    <h1>Cover Type</h1>
                    <div class="question left">
                        <a class="button default active" onclick="specs[0] ='Hardcover';">
                            Hardcover</a>
                        <a class="button" onclick="specs[0] ='Softcover';">
                            Softcover</a>
                    </div>
                </div>
            </div>
            <!-- Product & Intro-->
            <div class="col-lg-4" id="productContainer">
                <div id="intro">
                    <h1 class="text-center">Let's get started!</h1>
                    <p class="text-center">Using our configurator
                        you can create the book that is perfect for you!</p>
                    <div id="helpOverlay">
                        <img src="images/intro.png" draggable="false" class="img-fluid">
                        <div id="help1" class="helpBtn" onmouseover="helpOverlay('help1')"><br>1
                        </div>
                        <div id="help2" class="helpBtn" onmouseover="helpOverlay('help2')"><br>2
                        </div>
                        <div id="help3" class="helpBtn" onmouseover="helpOverlay('help3')"><br>3
                        </div>
                        <div id="help33" class="helpBtn" onmouseover="helpOverlay('help3')"><br>3
                        </div>
                        <div id="help4" class="helpBtn" onmouseover="helpOverlay('help4')"><br>4
                        </div>
                        <div id="help5" class="helpBtn" onmouseover="helpOverlay('help5')"><br>5
                        </div>
                        <div id="help6" class="helpBtn" onmouseover="helpOverlay('help6')"><br>6
                        </div>
                        <div id="help7" class="helpBtn" onmouseover="helpOverlay('help7')"><br>7
                        </div>
                        <div id="help8" class="helpBtn" onmouseover="helpOverlay('help8')"><br>8
                        </div>
                        <div id="help9" class="helpBtn" onmouseover="helpOverlay('help9')"><br>9
                        </div>
                        <div id="help10" class="helpBtn" onmouseover="helpOverlay('help10')"><br>10
                        </div>
                    </div>
                </div>
                <!-- Product field-->
                <div id="product" class="product">
                    <div class="glossSpine"></div>
                    <div id="elasticBand"></div>
                    <div id="brandLogo"></div>
                    <div id="penLoop"></div>
                </div>
            </div>
            <!-- steps right-->
            <div class="col-lg-3" id="stepsRight">
                <!-- Intro-->
                <div class="step" id="step0">
                </div>
                <!-- Cover + Corners -->
                <div class="step" id="step1">
                    <h1>Book Corners</h1>
                    <div class="question">
                        <a class="button" onclick="$('#product').css('border-radius', '0px 0px 0px 0px'), specs[1] ='Straight corners';">
                            Straight Corners</a>
                        <a class="button default active" onclick="$('#product').css('border-radius', '0px 24px 24px 0px'),
                         specs[1] ='Round corners';"> Round Corners</a>
                    </div>
                </div>
                <!-- Format -->
                <div class="step" id="step2">
                    <h1>Book Format</h1>
                    <div class="question">
                        <a class="button" onclick="formatVisual(a4),specs[2] = 'A4';">
                            A4 </a>
                        <a class="button default active" onclick="formatVisual(a5),specs[2] = 'A5';">
                            A5 </a>
                        <a class="button" onclick="formatVisual(a6),specs[2] = 'A6';">
                            A6 </a>
                        <div id="formats">
                            <div id="a4" class="formatVisual"></div>
                            <div id="a5" class="formatVisual active"></div>
                            <div id="a6" class="formatVisual"></div>
                        </div>
                    </div>
                </div>
                <!-- Type of cover step-->
                <div class="step" id="step3">
                    <h1 class="headerText">Cover Type</h1>
                </div>
                <!--Color pick step-->
                <div class="step" id="step4">
                    <h1> Choose a material </h1>
                    <div class="question">
                        <a class="button default active" data-value="0" id="all">All </a>
                        <a class="button" data-value="1" id="linnen" onclick="refreshQuery()">Linnen </a>
                        <a class="button" data-value="2">Soft PU </a>
                        <a class="button" data-value="3">Hard PU </a>
                        <a class="button" data-value="4">Eco </a>

                    </div>
                    <div id="demo"></div>
                    <div id="colorGallery" class="colorBallMenu">
                        <?php
                        /* If statement voor het oproepen van rijen uit Database, Als het aantal rijen groter is dan 1 */
                        if (mysqli_num_rows($result) >= 1) {
                            /* Als if statement = true dan roept hij de onderstaande rijen op */
                            while ($row = mysqli_fetch_assoc($result)) { 
                                /* Weergeven van de data in het tabel */
                                $productDiv = '"product"';
                                echo "<div class='colorBall" . $row["class"] . "' style='background-image:" . $row['coverPath'] . ";' 
                            onclick='specs[3] =" . $row['coverName'] . ",color =\"" . strval($row['coverPath']) . "\",changeColor();'>
                            <i class='fas fa-check fa-2x'></i>
                            </div>";
                            }
                        } else {
                            /* Wanneer de query mislukt toont hij: Error */
                            echo "Error occured";
                        }
                        ?>
                    </div>

                </div>
                <!-- Branding step-->
                <div class="step" id="step5">
                    <h1>Branding</h1>
                    <div class="question">
                        <a class="button" onclick="specs[4] = 'Pressed logo',
                        $('#brandLogo').fadeToggle();">
                            Pressed logo</a>
                        <a class="button" onclick="specs[4] = 'Foil pressed',
                        $('#brandLogo').fadeToggle();">
                            Foil Pressed</a>
                        <a class="button" onclick="specs[4] = 'Zeefdruk';">
                            Zeefdruk 1 </a>
                        <a class="button" onclick="specs[4] = 'Zeefdruk 2';">
                            Zeefdruk 2 </a>
                        <a class="button" onclick="specs[4] = 'Digitaaldruk (FC)';">
                            Digitaaldruk (FC) </a>
                        <a class="button" onclick="specs[4] = 'Spot-UV';">
                            Spot-UV </a>
                    </div>
                </div>
                <!-- Accessoires step -->
                <div class="step" id="step6">
                    <h1>Accessoires</h1>
                    <div class="question">
                        <a class="button" onclick='$("#elasticBandMenu").slideToggle(),
                        specs[5]="Aliceblue elastic band",$("#elasticBand").fadeToggle();'>
                            Elastic band
                        </a>
                        <!-- Color Elastic band -->
                        <div id="elasticBandMenu" class="colorBallMenu">
                            <div class="colorBall" style="background:blue;" onclick="document.getElementById('elasticBand').style.backgroundColor= 'blue', specs[5]='Blue elastic band';">
                                <i class='fas fa-check fa-2x'></i></div>
                            <div class="colorBall defaultBall activeBall" style="background:aliceblue;" onclick="document.getElementById('elasticBand').style.backgroundColor= 'aliceblue', specs[5]='Aliceblue elastic band';">
                                <i class='fas fa-check fa-2x'></i></div>
                            <div class="colorBall" style="background:#4C4C4C;" onclick="document.getElementById('elasticBand').style.backgroundColor= '#4C4C4C', specs[5]='Gray elastic band';">
                                <i class='fas fa-check fa-2x'></i></div>
                            <div class="colorBall" style="background:#B4DA55;" onclick="$('#elasticBand').css('background-color', '#B4DA55');">
                                <i class='fas fa-check fa-2x'></i></div>
                        </div>
                        <a class="button" onclick='specs[5] = "Pen Loop",$("#penLoopMenu").slideToggle(),
                        specs[5]="Aliceblue elastic band",$("#penLoop").fadeToggle();'>
                            Pen Loop </a>
                        <div id="penLoopMenu" class="colorBallMenu">
                            <div class="colorBall" style="background:blue;" onclick="$('#penLoop').css('background-color', 'blue');">
                                <i class='fas fa-check fa-2x'></i></div>
                            <div class="colorBall defaultBall activeBall" style="background:aliceblue;" onclick="$('#penLoop').css('background-color', 'aliceblue');">
                                <i class='fas fa-check fa-2x'></i></div>
                            <div class="colorBall" style="background:#4C4C4C;" onclick="$('#penLoop').css('background-color', '#4C4C4C');">
                                <i class='fas fa-check fa-2x'></i></div>
                            <div class="colorBall" style="background:#B4DA55;" onclick="$('#penLoop').css('background-color', '#B4DA55');">
                                <i class='fas fa-check fa-2x'></i></div>
                        </div>
                        <a class="button" onclick='specs[5] += "Banderol";'>
                            Banderol </a>
                    </div>
                </div>
                <!-- interior step -->
                <div class="step" id="step7">
                    <h1>Interior</h1>
                    <div class="question">
                        <a class="button" onclick=' specs[7]="page ribbon" ;'>
                            Page ribbon </a>
                        <a class="button" onclick='specs[7]="Storage compartment";'>
                            Storage comparment </a>
                    </div>
                </div>
                <!-- Frond/ endleave step -->
                <div class="step" id="step8">
                    <h1>Front/endleave</h1>
                    <div class="question">
                        <a class="button default active" onclick=''>
                            Colour </a>
                        <a class="button" onclick=''>
                            Printed </a>
                    </div>
                </div>
                <!-- book block step -->
                <div class="step" id="step9">
                    <h1>Book block</h1>
                    <div class="question">
                        <!-- Paper-->
                        <label for="paper">Paper:</label>
                        <select name="paper" id="paper" onchange="">
                            <option selected default> White paper</option>
                            <option> Brown paper</option>
                            <option> Sketch paper</option>
                            <option> Recycled paper</option>
                        </select>
                        <!-- Layout-->
                        <label for="layout">Lay-out:</label>
                        <select name="layout" id="layout" onchange="">
                            <option selected default> Blanco</option>
                            <option> Dots</option>
                            <option> Lines</option>
                            <option> Squares</option>
                        </select>
                        <!-- Pages-->
                        <label for="pages">Pages:</label>
                        <select name="layout" id="layout" onchange="">
                            <option selected default> 100 pages</option>
                            <option> 160 pages</option>
                            <option> 200 pages</option>
                            <option> 256 pages</option>
                        </select>
                    </div>
                </div>
                <!-- Extra pages step -->
                <div class="step" id="step10">
                    <h1>Extra pages</h1>
                    <div class="question">
                        <a class="button" onclick='$("#optionsAdPages").slideToggle();'>
                            Advertising pages </a>
                        <div id="optionsAdPages">
                            <label for="pagesAd">Pages:</label>
                            <select name="pagesAd" id="pagesAd" onchange="">
                                <option selected default> 4 pages</option>
                                <option> 8 pages</option>
                                <option> 16 pages</option>
                                <option> 32 pages</option>
                            </select>
                            <label for="locationAd">Location:</label>
                            <select name="locationAd" id="locationAd" onchange="">
                                <option selected default>In front of the book block</option>
                                <option>After the book block</option>
                                <option>Spread over the book block </option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Reviewing step -->
                <div class="step" id="step11">
                    <h1>Let's review!</h1>
                    <div id="specs">

                        <ul>
                            <li>Cover:<br> <span id="coverSpan"></span></li>
                            <li>Corners:<br> <span id="cornersSpan"></span></li>
                            <li>Format:<br> <span id="formatSpan"></span></li>
                            <li>Color:<br> <span id="colorSpan"></span></li>
                            <li>Branding:<br> <span id="brandingSpan"></span></li>
                            <li>Accessories:<br> <span id="accessoriesSpan"></span></li>
                            <li>Lay-out:<br> <span id="layoutSpan"></span></li>
                        </ul>

                        <button onclick="nextPrev(1)">Confirm</button>
                    </div>
                </div>
                <!-- Submit step -->
                <div class="step" id="step12">

                </div>
                <!-- Close collum tag -->
            </div>
            <!-- Next button-->
            <div class="col-lg-1">
                <div id="nextbtn" onclick="nextPrev(1);">
                    <i class="fas fa-chevron-right fa-3x"></i>
                </div>
                <div id="buttonBar">
                    <i id="cellphone" class="fas fa-phone-alt fa-2x " onclick="window.open('tel:+31727112998');"></i>
                    <i id="help" class="fas fa-question-circle fa-2x " onclick="savePos= currentStep;nextPrev(-currentStep)"></i>
                    <i id="savePos" class="fas fa-undo-alt fa-2x fa-flip-horizontal" onclick="nextPrev(savePos-currentStep), savePos = 0, $('#savePos').hide();"></i>
                    <i id="reset" class="fas fa-trash-alt fa-2x" onclick="resetDisplayOn()"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid fixed-bottom">
        <div class="col-lg-12">
            <div id="copyright"> <i class="far fa-copyright"></i> Leoprinting 2020</div>
        </div>
    </div>
</body>
<script src="./js/functions.js"></script>
<script src="./js/utils.js"></script>
<script type="text/javascript">
    $(function(){
        $('#step4').on('click', '.question a', function(){
            var textureId = $(this).data('value');
            $.ajax({
                url: "php/get-material.php",
                method: "POST",
                data: { sort: textureId },
                dataType: "JSON",
                success: function(data){
                    var layout = new Array();
                    $.each(data, function(key, value){
                        layout.push("<div class='colorBall "+value.class+"' style='background-image:"+value.coverPath+"' onclick='specs[3] ="+value.coverName+", color=\""+value.coverPath+"\", changeColor();'><i class='fas fa-check fa-2x'></i></div>");
                    })
                    $('#demo').html(layout);
                }
            });
        });
    });
</script>
</html>