-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 15 mei 2020 om 11:37
-- Serverversie: 10.4.6-MariaDB
-- PHP-versie: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `configurator`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `covercolor`
--

CREATE TABLE `covercolor` (
  `coverID` int(11) NOT NULL,
  `coverName` varchar(255) DEFAULT NULL,
  `coverPath` varchar(255) DEFAULT NULL,
  `colorID` int(3) DEFAULT NULL,
  `textureID` int(3) DEFAULT NULL,
  `timesChosen` int(11) DEFAULT 0,
  `priceID` int(11) DEFAULT NULL,
  `class` varchar(65) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `covercolor`
--

INSERT INTO `covercolor` (`coverID`, `coverName`, `coverPath`, `colorID`, `textureID`, `timesChosen`, `priceID`, `class`) VALUES
(1, '\"Greyscale\"', 'url(../configurator/images/covers/grey1.svg)', 1, 1, 0, 4, ' defaultBall activeBall'),
(2, '\"Raging Red\"', 'url(../configurator/images/covers/red1.svg)', 2, 4, 0, 5, ''),
(3, '\"Playful Purple\"', 'url(../configurator/images/covers/purple1.svg)', 3, 1, 0, 4, ''),
(4, '\"Beautiful Blue\"', 'url(../configurator/images/covers/blue1.svg)', 4, 1, 0, 4, ''),
(6, '\"Terrific Teal\"', 'url(../configurator/images/covers/teal1.svg)', 4, 1, 0, 4, ''),
(7, '\"Luxurious Leather\"', 'url(../configurator/images/covers/leather1.svg)', 5, 3, 0, 4, ''),
(8, '\"Breathtaking Brown\"', 'url(../configurator/images/covers/brown1.svg)', 6, 3, 0, 5, ''),
(9, '\"Troublesome Teal\"', 'url(../configurator/images/covers/teal2.svg)', 4, 1, 0, 4, ''),
(10, '\"Grazing Grey\"', 'url(../configurator/images/covers/grey2.svg)', 1, 1, 0, 4, ''),
(11, '\"Precious Pink\"', 'url(../configurator/images/covers/pink1.svg)', 7, 1, 0, 5, ''),
(12, '\"Yappy Yellow\"', 'url(../configurator/images/covers/yellow1.svg)', 8, 1, 0, 5, ''),
(13, '\"Baby Blue\"', 'url(../configurator/images/covers/blue2.svg)', 4, 2, 0, 5, '');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `prices`
--

CREATE TABLE `prices` (
  `priceID` int(11) NOT NULL,
  `price` decimal(6,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `prices`
--

INSERT INTO `prices` (`priceID`, `price`) VALUES
(4, '3.50'),
(5, '2.50');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `covercolor`
--
ALTER TABLE `covercolor`
  ADD PRIMARY KEY (`coverID`),
  ADD KEY `FK` (`priceID`);

--
-- Indexen voor tabel `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`priceID`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `covercolor`
--
ALTER TABLE `covercolor`
  MODIFY `coverID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT voor een tabel `prices`
--
ALTER TABLE `prices`
  MODIFY `priceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
